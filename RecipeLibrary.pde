/* RecipeLibrary Class
 * Author: Matthew Rowley
 * 
 * This class will import all recipes and ingredients from human-
 * readable text files. It will also provide the appropriate names
 * to display on list selection buttons.
 *
 * Licence information is at the bottom
 */

class RecipeLibrary {
  ScreenData screenData;
  ArrayList<Recipe> recipeList;
  StringList recipes;
  StringList ingredients;
  StringList types;
  StringList[] recipesByIngredient;
  StringList[] recipesByType;
  StringList currentMenu;
  ArrayList<Ingredient> inventory;
  // Constructor
  RecipeLibrary(ScreenData isd) {
    screenData = isd;
    recipeList = new ArrayList<Recipe>();
    recipes = new StringList();
    ingredients = new StringList();
    types = new StringList(); 
    currentMenu = new StringList();
    // Import all the recipe files in the recipe folder
    File recipeFolder = new File(sketchPath("Recipes"));
    getRecipeFiles(recipeFolder);

    // Create tier 1 button name lists
    for (int i = 0; i<recipeList.size(); i++) { 
      Recipe thisRecipe = recipeList.get(i);
      String thisName = thisRecipe.getRecipeName();
      ArrayList<Ingredient> theseIngredients = thisRecipe.getIngredients();
      String thisType = thisRecipe.getRecipeType();
      if (!recipes.hasValue(thisName)) { // repeat recipe names will not be added
        recipes.append(thisName);
        for (int j = 0; j<theseIngredients.size(); j++) {
          if (!ingredients.hasValue(theseIngredients.get(j).getIngredientName())) {
            ingredients.append(theseIngredients.get(j).getIngredientName());
          }
        }
        if (!types.hasValue(thisType)) {
          types.append(thisType);
        }
      } // end of if(!recipes.hasValue(thisName))
      else { 
        String removedName = recipeList.remove(i).getRecipeName(); // Here repeat names are removed from the recipeList
        println("The recipe: " + removedName + " was not added because it was a duplicate");
        i--; // i is decremented so it will not increment - the element was removed so we don't actually want the next index
      }
    } // end of creating tier 1 lists
    recipes.sort();
    ingredients.sort();

    // Create tier 2 button name lists
    recipesByIngredient = new StringList[ingredients.size()];
    for (int i = 0; i<recipesByIngredient.length;i++) {
      recipesByIngredient[i] = new StringList();
    }
    // Get recipes by ingredient
    for (int i = 0; i < ingredients.size(); i++) {
      for (int j = 0; j < recipes.size(); j++) {
        Recipe thisRecipe = recipeList.get(j);
        if (thisRecipe.hasIngredient(ingredients.get(i))) {
          recipesByIngredient[i].append(thisRecipe.getRecipeName());
        }
      }
      recipesByIngredient[i].sort();
    }
    types.sort();
    recipesByType = new StringList[types.size()];
    for (int i = 0; i<recipesByType.length;i++) {
      recipesByType[i] = new StringList();
    }
    // Get recipes by type
    for (int i = 0; i < types.size(); i++) {
      for (int j = 0; j < recipes.size(); j++) {
        Recipe thisRecipe = recipeList.get(j);
        if (thisRecipe.getRecipeType().equals(types.get(i))) {
          recipesByType[i].append(thisRecipe.getRecipeName());
        }
      }
      recipesByType[i].sort();
    }

    importInventory();
  }// End constructor

  void getRecipeFiles(File folder) {
    String[] names = folder.list();
    for (int i=0; i<names.length; i++) {
      File thisFile = new File(folder, names[i]);
      if (thisFile.isDirectory()) {
        getRecipeFiles(thisFile);
      }
      else if (names[i].substring(names[i].length()-4, names[i].length()).equals(".rcp")) {
        recipeList.add(new Recipe(thisFile));
      }
    }
  }
  void importInventory() {
    inventory = new ArrayList<Ingredient>();
    File inventoryFolder = new File(sketchPath("Inventory"));
    File inventoryFile = new File(inventoryFolder, "Inventory.inv");
    String[] lines = loadStrings(inventoryFile);
    int currentLine = 2;
    while (!lines[currentLine].equals ("--------------------------------------------------")) {
      inventory.add(new Ingredient(lines[currentLine]));
      currentLine++;
    }
  }
  Recipe getRecipe(String recipeName) {
    Recipe temp = recipeList.get(0);
    for (int i = 0; i<recipeList.size();i++) {
      temp = recipeList.get(i);
      if (temp.getRecipeName().equals(recipeName)) {
        break;
      }
    }
    return temp;
  }
  void prepareMenu() {
    if (screenData.getMode() == 1) {
      currentMenu = ingredients;
    }
    else if (screenData.getMode() == 2) {
      currentMenu = types;
    }
    else if (screenData.getMode() == 3) {
      currentMenu = recipes;
    }
    else if (screenData.getMode() == 4) {
      String lastPressed = screenData.getLastPressed();
      int index = 0;
      for (int i = 0; i< ingredients.size(); i++) {
        if (ingredients.get(i).equals(lastPressed)) {
          index = i;
        }
      }
      currentMenu = recipesByIngredient[index];
    }
    else if (screenData.getMode() == 5) {
      String lastPressed = screenData.getLastPressed();
      int index = 0;
      for (int i = 0; i< types.size(); i++) {
        if (types.get(i).equals(lastPressed)) {
          index = i;
        }
      }
      currentMenu = recipesByType[index];
    }
    int pageMax = -1;
    for (int i = 0; i < currentMenu.size(); i = i +6){
      pageMax++;
    } 
    screenData.setPageMax(pageMax);
  }
  Ingredient getIngredient(String name){
    Ingredient thisIngredient = new Ingredient("Fake - 1 [fake][fake]");
    for(int i = 0; i<inventory.size(); i++){
      if(name.equals(inventory.get(i).getInventoryName())){
        thisIngredient = inventory.get(i);
      }  
    }
    return thisIngredient;
  }
  String[] getInventoryNames() {
    String[] names = new String[inventory.size()];
    for (int i = 0; i< inventory.size(); i++) {
      names[i] = inventory.get(i).getInventoryName();
    }
    return names;
  }
  int[] getInventoryNumbers() {
    int[] numbers = new int[inventory.size()];
    for (int i = 0; i< inventory.size(); i++) {
      numbers[i] = int(inventory.get(i).getIngredientAmount());
    }
    return numbers;
  }
  void writeInventory(){
    File inventoryFile = new File(sketchPath("Inventory/Inventory.inv"));
    String[] lines = loadStrings(inventoryFile);
    int currentLine = 2;
    while (!lines[currentLine].equals ("--------------------------------------------------")) {
      lines[currentLine] = inventory.get(currentLine-2).getRawData();
      currentLine++;
    }
    saveStrings(inventoryFile.getAbsolutePath(), lines);
  }
  String[] getButtonNames() {
    String[] buttonNames = currentMenu.array();
    return buttonNames;
  }
}

/* This file is part of Kitchen_Aide.
 *
 * Kitchen_Aide is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Kitchen_Aide is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser Public License for more details.
 *
 * You should have received a copy of the GNU Lesser Public License
 * along with Kitchen_Aide.  If not, see <http://www.gnu.org/licenses/>.
 */
