/* Recipe Class
 * Author: Matthew Rowley
 * 
 * This class contains all the information about a recipe
 *
 * Licence information is at the bottom
 */
 class Recipe{
   String[] fileLines;
   String recipeName;
   String recipeType;
   float recipeYield;
   String yieldUnit;
   String yieldUnits;
   ArrayList<Ingredient> ingredients;
   String instructions;
   int currentLine; // the first line of ingredients
   String[] lineTokens;
   Recipe(File recipeFile){
     instructions = "";
     ingredients = new ArrayList<Ingredient>();
     fileLines = loadStrings(recipeFile.getAbsolutePath());
     recipeName = fileLines[1];
     recipeType = fileLines[3];
     lineTokens = split(fileLines[5], '[');
     recipeYield = parseFloat(split(lineTokens[0],' ')[1]);
     yieldUnit = lineTokens[1].substring(0,lineTokens[1].length()-1);
     yieldUnits = lineTokens[2].substring(0,lineTokens[2].length()-1);
     currentLine = 8;
     while(!fileLines[currentLine].equals("-----------------------------------------------------------------")){
       ingredients.add(new Ingredient(fileLines[currentLine]));
       currentLine++;
     }
     currentLine = currentLine + 2;
     while(!fileLines[currentLine].equals("=================================================================")&&currentLine<fileLines.length){ // Check the current line in case the .rcp file doesn't end in a line of dashes
       instructions = instructions + fileLines[currentLine] + "\n";
       currentLine++;
     }
   }
   String getRecipeName(){
     return recipeName;
   }
   String getRecipeType(){
     return recipeType;
   }
   String getRecipeYield(float multiplier){
     String yield;
     float number = recipeYield*multiplier;
     if(number == 1 || number <= 0.59){
       yield = "Yields:                            " + formatNumber(number) + " " + yieldUnit;
     }
     else{
       yield = "Yields:                            " + formatNumber(number) + " " + yieldUnits;
     }
     if(multiplier != 1){
       yield = yield + " (x" + formatNumber(multiplier) + ")";
     }
     return yield;
   }
   
   ArrayList<Ingredient> getIngredients(){
     return ingredients;
   }
   boolean hasIngredient(String ingredient){
     boolean doesHave = false;
     for (int i = 0; i<ingredients.size(); i++){
       doesHave = doesHave || ingredients.get(i).getIngredientName().equals(ingredient);
     }
     return doesHave;
   }
   String getInstructions(){
     return instructions;
   }
   String formatNumber(float number){
     float remainder = number - int(number);
     String remainderString;
     if(remainder <0.075){
       remainderString = "";
     }
     else if(remainder <= 0.175){
       remainderString = " 1/8";
     }
     else if(remainder <= 0.29){
       remainderString = " 1/4";
     }
     else if(remainder <= 0.41){
       remainderString = " 1/3";
     }
     else if(remainder <= 0.59){
       remainderString = " 1/2";
     }
     else if(remainder <= 0.71){
       remainderString = " 2/3";
     }
     else if(remainder <= 0.85){
       remainderString = " 3/4";
     }
     else{
       number++;
       remainderString = "";
     }
     if(int(number)== 0){
       return remainderString.substring(1);
     }
     else{
       return int(number) + remainderString;
     }
   }
 }
 
/* This file is part of Kitchen_Aide.
 *
 * Kitchen_Aide is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Kitchen_Aide is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser Public License for more details.
 *
 * You should have received a copy of the GNU Lesser Public License
 * along with Kitchen_Aide.  If not, see <http://www.gnu.org/licenses/>.
 */
