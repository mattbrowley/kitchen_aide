/* Button Class
 * Author: Matthew Rowley
 * 
 * This class is a generalized button
 *
 * Licence information is at the bottom
 */

class Button {
  int xpos; // Note that these are center positions
  int ypos;
  int wideness; // Note that these are "radii" (1/2 the wideness and 1/2 the highness
  int highness;
  color buttonColor;
  String name; // The text displayed on the button and an identifier to signal what button was last pressed
  boolean enabled; // A button can be disabled so it isn't displayed and cannot act
  ScreenData screenData;
  // Dummy Constructor
  Button() {
    enabled = false;
  }
  // Main Constructor
  Button(int ixpos, int iypos, int iwide, int ihigh, String ina, ScreenData isd) {
    xpos = ixpos;
    ypos = iypos;
    wideness = iwide;
    highness = ihigh;
    name = ina;
    screenData = isd;
    buttonColor = color(210, 210, 240);
    enabled = true; // Allows a button to not be drawn if false
  }
  void press() { // Checks if the mouse is over this particular button, and the button is enabled
    if (mouseX>xpos-wideness && mouseX<xpos+wideness && mouseY>ypos-highness && mouseY<ypos+highness && enabled) {
      act();
    }
  }
  void setButtonName(String setValue) {
    name = setValue;
  }
  void act() {
    screenData.setLastPressed(name);
  }
  void setEnabled(boolean setValue) {
    enabled = setValue;
  }
  void setColor(color setColor){
    buttonColor = setColor;
  }
  void drawButton() {
    if (enabled) {
      fill(buttonColor);
      rect(xpos-wideness, ypos-highness, 2 * wideness, 2 * highness, 20, 20, 20, 20);
      fill(50); // Dark Gray Letters
      textFont(boldFont);
     // textAlign(LEFT,CENTER);
      String displayName = name;
      boolean splitName = false;
      for (int i = 0; i<name.length(); i++){
        if (name.charAt(i) == '\n'){
          splitName = true;
        }
      }
      if (name.length()>25 && !splitName){ // Break the name in to two lines in a logical way
        for (int i = 18; i < 30; i++) {
          if (name.charAt(i) == ' ') {
            displayName = name.substring(0, i) + "\n" + name.substring(i+1);
            break;
          }
          if (i == 30) {
            displayName = name.substring(0, 20) + "-\n" + name.substring(20);
          }
        }
      }
      text(displayName, xpos, ypos);
     // textAlign(CENTER,CENTER);
    }
  }
}


/* This file is part of Kitchen_Aide.
 *
 * Kitchen_Aide is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Kitchen_Aide is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser Public License for more details.
 *
 * You should have received a copy of the GNU Lesser Public License
 * along with Kitchen_Aide.  If not, see <http://www.gnu.org/licenses/>.
 */
