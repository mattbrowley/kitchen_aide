============================================================
Kitchen-Aide Recipe Manager and Kitchen Inventory
============================================================
Author: Matthew Rowley
------------------------------------------------------------
This program creates a browse-able database of recipes from
human-readable plaintext ".rcp" recipe files. Recipes can be
doubled, halved, etc.

It is also a kitchen inventory which allows you to add and
remove ingredients, as well as view the current inventory in
a convenient way. 
--------------------Writing a .rcp File---------------------
To include a new recipe in the database you must compose the
".rcp" file according to the template provided in the file 
"Recipe_Template.rcp".
The following specific features are essential:
1. The second line should contain the recipe title with no
   extra whitespace.
2. The third line line should describe the type of recipe,
   such as "Entree", "Dessert", "Appetizer", etc. Note that
   it is this field that determines the recipe type in the
   database, not the name of the folder where the recipe
   file is found. The folder structure was implemented 
   merely for user convenience.
3. The sixth line should contain the text "Yields: ", then a
   number, a space, and the units. Units should be written
   in singular and plural forms enclosed in square brackets.
   For example:
   "Yields: 4 [Serving][Servings]"
   "Yields: 3.5 [Cup][Cups]"
   "Yields: 24 [Small Cookie][Small Cookies]"
4. Starting on the ninth line, each ingredient should be on 
   a new line. The name of the ingredient comes first, and
   may be any text except " - ", which signals the end of
   the ingredient name. The amount comes next as any number
   followed by any text to describe the units in the same
   way as for the yield. For example:
   "Cheese - 1.5 [cup][cups]"
   "Cream of Chicken Soup - 1 [14 oz can][14 oz cans]"
5. After the last ingredient, a line of 65 "-" characters
   marks the beginning of the instructions. The next line is
   then skipped and should contain text meant for human
   readers such as "Instructions:"
6. Instructions occupy the following lines. They should be
   manually formatted to have 65 characters per line. This
   is calibrated to fill the screen based on my 480 x 800
   vertically oriented display. Of course, if you will use a
   different display this should be modified in both the
   recipe ".rcp" files and numerous places in the source
   code.
7. The end of the instructions (and the end of the recipe)
   is marked by a line of 65 "=" characters.
--------------------Writing a .inv File---------------------
The .inv file is simpler. The first two lines are ignored by
the Kitchen-Aide program, and are usually used to warn the
user that the inventory will be displayed in the order in 
the file, rather than alphabetical order.
The last line should be a line of 65 "-" characters to mark
the end of the inventory.
All other lines should have: 
1. Ingredient Name
2. " - " to separate the name from the rest
3. An initial number. This is modified when you add/remove 
   in the application
4. Singular and plural units in the form [singular][plural]
