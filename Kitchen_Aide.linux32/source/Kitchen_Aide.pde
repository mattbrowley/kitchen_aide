/* Kitchen_Aide
 * Author: Matthew Rowley
 * Date Modified: 03/26/2014
 * 
 * A recipe manager and kitchen inventory designed for a small touchscreen.
 * This program will recall and display recipes stored in human-readable
 * plaintext files. Other useful kitchen features such as a recipe multiplier
 * and kitchen timer will be added later.
 *
 * Licence information is at the bottom
 */

ScreenData screenData; // This encapsulates the screen mode, page on each screen, and last button pressed
int mode;

RecipeLibrary recipes;
Recipe currentRecipe;
Ingredient currentIngredient;
float[] yieldMultipliers = {8,4,3,2.5,2,1.75,1.5,1.25,1,0.75,0.666,0.5,0.333,0.25,0.125};
PFont logoFont;
PFont smallLogoFont;
PFont boldFont;
PFont textFont;

Button[] buttons0 = new Button[5];
Button[] buttons1 = new Button[9];
Button[] buttons2 = new Button[9];
Button[] buttons3 = new Button[9];
Button[] buttons4 = new Button[9];
Button[] buttons5 = new Button[9];
Button[] buttons6 = new Button[3];
Button[] buttons7 = new Button[9];
Button[] buttons8 = new Button[4];
Button[][] Buttons = {buttons0, buttons1, buttons2, buttons3, buttons4, buttons5, buttons6, buttons7, buttons8};
String[] buttonNames;

void setup(){
  noLoop();
  size(600,800);
  screenData = new ScreenData();
                                       // 0 - Home Screen
                                       // 1 - Ingredients
                                       // 2 - Types
                                       // 3 - Recipes by Name
                                       // 4 - Recipes by Ingredient
                                       // 5 - Recipes of Type
                                       // 6 - Recipe
                                       // 7 - Inventory
                                       // 8 - Add/Remove from Inventory
   recipes = new RecipeLibrary(screenData);
  logoFont = loadFont("TeXGyreChorus-MediumItalic-70.vlw");
  smallLogoFont = loadFont("TeXGyreTermes-Italic-25.vlw");
  boldFont = loadFont("Courier10PitchBT-Bold-30.vlw");
  textFont = loadFont("ArialMT-20.vlw");
  textAlign(CENTER, CENTER); // Position text in the center of buttons
   
  Buttons[0][0] = new ModeButton(width/2, 1*(height-300)/8+220 ,width/2 - 10, (height-340) / 8, "Recipes by Name", screenData, 3);
  Buttons[0][1] = new ModeButton(width/2, 3*(height-300)/8+220, width/2 - 10, (height-340) / 8, "Recipes by Ingredient", screenData, 1);
  Buttons[0][2] = new ModeButton(width/2, 5*(height-300)/8+220, width/2 - 10, (height-340) / 8, "Recipes by Type", screenData, 2);
  Buttons[0][3] = new ModeButton(width/2, 7*(height-300)/8+220, width/2 - 10, (height-340) / 8, "Manage Inventory", screenData, 7);
  Buttons[0][4] = new ExitButton(width - 45, height - 45, 35, 35, "Exit", screenData);
  
  for (int i = 1; i<3; i++){ // Set up buttons for the list selection screens (1-2)
    Buttons[i][0] = new UpButton(width/2-40, 40, width/2-60, 30, "Up", screenData);
    Buttons[i][1] = new DownButton(width/2-40, height-40, width/2-60, 30, "Down", screenData);
    Buttons[i][2] = new HomeButton(width-40, 40, 30, 30, "Home", screenData, 0);
    Buttons[i][3] = new ModeButton(width/2, 1*(height-150)/12+75, width/2 - 10, (height-190) / 12, "button "+ i + ".1", screenData, i+3);
    Buttons[i][4] = new ModeButton(width/2, 3*(height-150)/12+75, width/2 - 10, (height-190) / 12, "button "+ i + ".2", screenData, i+3);
    Buttons[i][5] = new ModeButton(width/2, 5*(height-150)/12+75, width/2 - 10, (height-190) / 12, "button "+ i + ".3", screenData, i+3);
    Buttons[i][6] = new ModeButton(width/2, 7*(height-150)/12+75, width/2 - 10, (height-190) / 12, "button "+ i + ".4", screenData, i+3);
    Buttons[i][7] = new ModeButton(width/2, 9*(height-150)/12+75, width/2 - 10, (height-190) / 12, "button "+ i + ".5", screenData, i+3);
    Buttons[i][8] = new ModeButton(width/2, 11*(height-150)/12+75, width/2 - 10, (height-190) / 12, "button "+ i + ".6", screenData, i+3);
  }
  for (int i = 3; i<6; i++){ // Set up buttons for the recipe selection screens (3-5)
    Buttons[i][0] = new UpButton(width/2-40, 40, width/2-60, 30, "Up", screenData);
    Buttons[i][1] = new DownButton(width/2-40, height-40, width/2-60, 30, "Down", screenData);
    Buttons[i][2] = new HomeButton(width-40, 40, 30, 30, "Home", screenData, 0);
    Buttons[i][3] = new ModeButton(width/2, 1*(height-150)/12+75, width/2 - 10, (height-190) / 12, "button "+ i + ".1", screenData, 6);
    Buttons[i][4] = new ModeButton(width/2, 3*(height-150)/12+75, width/2 - 10, (height-190) / 12, "button "+ i + ".2", screenData, 6);
    Buttons[i][5] = new ModeButton(width/2, 5*(height-150)/12+75, width/2 - 10, (height-190) / 12, "button "+ i + ".3", screenData, 6);
    Buttons[i][6] = new ModeButton(width/2, 7*(height-150)/12+75, width/2 - 10, (height-190) / 12, "button "+ i + ".4", screenData, 6);
    Buttons[i][7] = new ModeButton(width/2, 9*(height-150)/12+75, width/2 - 10, (height-190) / 12, "button "+ i + ".5", screenData, 6);
    Buttons[i][8] = new ModeButton(width/2, 11*(height-150)/12+75, width/2 - 10, (height-190) / 12, "button "+ i + ".6", screenData, 6);
  }
  Buttons[6][0] = new HomeButton(width-40, 40, 30, 30, "Home", screenData, 0);
  Buttons[6][1] = new UpButton(151, 118, 41, 19, "Up", screenData); // Increase yieldmultiplier
  Buttons[6][2] = new DownButton(100, 118, 41, 19, "Down", screenData); // Decrease yieldmultiplier
  Buttons[7][0] = new UpButton(width/2-40, 40, width/2-60, 30, "Up", screenData);
  Buttons[7][1] = new DownButton(width/2-40, height-40, width/2-60, 30, "Down", screenData);
  Buttons[7][2] = new HomeButton(width-40, 40, 30, 30, "Home", screenData, 0);
  Buttons[7][3] = new ModeButton(width/2, 1*(height-150)/12+75, width/2 - 10, (height-190) / 12, "button 7.1", screenData,8);
  Buttons[7][4] = new ModeButton(width/2, 3*(height-150)/12+75, width/2 - 10, (height-190) / 12, "button 7.2", screenData,8);
  Buttons[7][5] = new ModeButton(width/2, 5*(height-150)/12+75, width/2 - 10, (height-190) / 12, "button 7.3", screenData,8);
  Buttons[7][6] = new ModeButton(width/2, 7*(height-150)/12+75, width/2 - 10, (height-190) / 12, "button 7.4", screenData,8);
  Buttons[7][7] = new ModeButton(width/2, 9*(height-150)/12+75, width/2 - 10, (height-190) / 12, "button 7.5", screenData,8);
  Buttons[7][8] = new ModeButton(width/2, 11*(height-150)/12+75, width/2 - 10, (height-190) / 12, "button 7.6", screenData,8);
  Buttons[8][0] = new Button(width/2, height/2, width/2-10, 60, "Inventory Item", screenData);
  Buttons[8][1] = new UpButton(width/2, height/2-120, 120,50, "up", screenData);
  Buttons[8][2] = new DownButton(width/2, height/2+120, 120,50, "down", screenData);
  Buttons[8][3] = new BackButton(width-70, 70, 60, 40, "Back", screenData, 7, recipes);
  
 
    
  background(190);
  fill(50);
  redraw();
} // End setup()
void draw(){
  background(190);
  if(mode != screenData.getMode()){ // 
    mode = screenData.getMode();
    if(mode > 0 && mode < 6){
      recipes.prepareMenu();
    }
    if(mode == 6){ // Set up some things for the drawRecipe() function
      currentRecipe = recipes.getRecipe(screenData.getLastPressed());
      screenData.setPage(8); // Start with a multiplier of 1
    }
    if(mode == 7){
      int pageMax = -1;
      for (int i = 0; i < recipes.getInventoryNumbers().length; i = i+6){
        pageMax++;
      } 
      screenData.setPageMax(pageMax);
    }
    if(mode == 8){
      currentIngredient = recipes.getIngredient(screenData.getLastPressed());
      screenData.setPage(999 - int(currentIngredient.getIngredientAmount()));
    }
  } // end if(modeChanged)
  if(mode==0){
    drawHome();
  }
  else if(mode==6){
    drawRecipe();
  }
  else if(mode==7){
    drawInventoryMenu();
  }
  else if(mode==8){
    drawChangeInventory();
  }
  else{
    drawMenu();
  }
  for (int i = 0; i < Buttons[mode].length; i++){
    Buttons[mode][i].drawButton();
  } 
} // End draw()

void mousePressed(){
  for (int i = 0; i < Buttons[mode].length;i++){
    Buttons[mode][i].press(); // Button objects determine whether the mouse is over them when the mouse is pressed
  }
  redraw();
}

// Draw functions for each mode are below
void drawHome(){
  fill(0,0,255);
  textFont(logoFont);
  text("Kitchen-Aide", width/2, 50);
  textFont(smallLogoFont);
  text("Recipe Manager and Kitchen Inventory", width/2, 110);
  textFont(boldFont);
  text("Written with Love by\nMatt Rowley", width/2, 180);
}
void drawRecipe(){
  float multiplier = yieldMultipliers[screenData.getPage()];
  int stretchSpace = 0;
  fill(0);
  textFont(boldFont);
  textAlign(LEFT,CENTER);
  String recipeName = currentRecipe.getRecipeName();
  if(recipeName.length()<25){
    text(recipeName, 10, 40);
    textFont(smallLogoFont);
    text(currentRecipe.getRecipeType(), 10, 80+stretchSpace);
  }
  else{ // Break the title in to two lines in a logical way
    for(int i = 18; i < 30; i++){
      if(recipeName.charAt(i) == ' '){
        recipeName = recipeName.substring(0,i) + "\n" + recipeName.substring(i+1);
        break;
      }
      if(i == 30){
        recipeName = recipeName.substring(0,20) + "-\n" + recipeName.substring(20);
      }
    }
    text(recipeName, 10, 40);
    textFont(smallLogoFont);
    text(currentRecipe.getRecipeType(), 10, 85+stretchSpace);
  } 
  textFont(textFont);
  text(currentRecipe.getRecipeYield(multiplier), 10, 118+stretchSpace);
  strokeWeight(3);
  line(5,140+stretchSpace,width-5,140+stretchSpace);
  textFont(smallLogoFont);
  text("Ingredients:", 10, 160 + stretchSpace);
  textFont(textFont);
  for (int i = 0; i < currentRecipe.getIngredients().size(); i++){
    text(currentRecipe.getIngredients().get(i).getRecipeName(multiplier), 10, 190+stretchSpace);
    stretchSpace += 20;
  }
  line(5,190+stretchSpace,width-5,190+stretchSpace);
  textFont(smallLogoFont);
  text("Instructions:", 10, 210+stretchSpace);
  textFont(textFont);
  textAlign(LEFT,TOP);
  text(currentRecipe.getInstructions(), 10, 230+stretchSpace);
  
 
  strokeWeight(1);
  textAlign(CENTER,CENTER);
}
void drawMenu(){ // Get the proper button names from the RecipeLibrary
  String[] buttonNames = recipes.getButtonNames();
  int page = screenData.getPage();
  for (int i = 0; i<6; i++){
    if(i+ 6*page < buttonNames.length){
      Buttons[mode][i+3].setButtonName(buttonNames[i+6*page]);
      Buttons[mode][i+3].setEnabled(true);
    }
    else{
      Buttons[mode][i+3].setEnabled(false);
    }
  } 
}
void drawInventoryMenu(){
  String[] buttonNames = recipes.getInventoryNames();
  int[] inventoryNumbers = recipes.getInventoryNumbers();
  int page = screenData.getPage();
  for (int i = 0; i<6; i++){
    if(i+ 6*page < buttonNames.length){
      Buttons[mode][i+3].setButtonName(buttonNames[i+6*page]);
      Buttons[mode][i+3].setEnabled(true);
      int number = inventoryNumbers[i+6*page];
      if(number == 0){
        Buttons[mode][i+3].setColor(color(240,68,68));
      }
      else if(number == 1){
         Buttons[mode][i+3].setColor(color(240,125,68));
      }
      else{
         Buttons[mode][i+3].setColor(color(210, 210, 240));
      }
    }
    else{
      Buttons[mode][i+3].setEnabled(false);
    }
  } 
}
void drawChangeInventory(){
  currentIngredient.setIngredientAmount(999 - screenData.getPage());
  int number = int(currentIngredient.getIngredientAmount());
    if(number == 0){
      Buttons[mode][0].setColor(color(240,68,68));
    }
    else if(number == 1){
       Buttons[mode][0].setColor(color(240,125,68));
    }
    else{
       Buttons[mode][0].setColor(color(210, 210, 240));
    }
  Buttons[mode][0].setButtonName(currentIngredient.getInventoryName());

}
/* This file is part of Kitchen_Aide.
 *
 * Kitchen_Aide is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Kitchen_Aide is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser Public License for more details.
 *
 * You should have received a copy of the GNU Lesser Public License
 * along with Kitchen_Aide.  If not, see <http://www.gnu.org/licenses/>.
 */
