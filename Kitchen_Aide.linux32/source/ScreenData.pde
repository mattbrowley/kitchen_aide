/* ScreenData Class
 * Author: Matthew Rowley
 * 
 * This class holds global variables used by other classes in the
 * Kitchen_Aide program. Namely, the screen mode, current page
 * for each screen, and name of the last button pushed. 
 *
 * Licence information is at the bottom
 */

class ScreenData{
  int mode;
  int[] pages = {0,0,0,0,0,0,8,0,999};
  int[] pagesMax = {0,0,0,0,0,0,14,0,999};
  boolean modeChanged;
  boolean pageChanged;
  String lastPressed;
  String currentRecipe;
  float yieldMultiplier;
  void ScreenData(){
    mode = 0;
    modeChanged = true;
    pageChanged = true;
    lastPressed = "Unchanged";
    currentRecipe = "Chicken Enchiladas"; // Perhaps change this to an emptry string once debugging is over
  }
  int getMode(){
    modeChanged = false;
    return mode;
  }
  void setMode(int setValue){
    mode = setValue;
    modeChanged = true;
  }
  boolean getModeChanged(){
    return modeChanged;
  }
  int getPage(){
    return pages[mode];
  }
  void setPage(int setValue){
    pages[mode] = setValue;
    pageChanged = true;
  }
  void incrementPage(){
    if (pages[mode]<pagesMax[mode]){
      pages[mode]++;
      pageChanged = true;
    }
  }
  void decrementPage(){
    if (pages[mode]>0){
      pages[mode]--;
      pageChanged = true;
    }
  }
  boolean getPageChanged(){
    return pageChanged;
  }
  void clearPageChanged(){
    pageChanged = false;
  }
  int getPageMax(){
    return pagesMax[mode];
  }
  void setPageMax(int setValue){
    pagesMax[mode] = setValue;
  }
  String getLastPressed(){
    return lastPressed;
  }
  void setLastPressed(String setValue){
    lastPressed = setValue;
  }
  String getCurrentRecipe(){
    return currentRecipe;
  }
  void selectCurrentRecipe(){
    currentRecipe = lastPressed;
  }
}

/* This file is part of Kitchen_Aide.
 *
 * Kitchen_Aide is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Kitchen_Aide is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser Public License for more details.
 *
 * You should have received a copy of the GNU Lesser Public License
 * along with Kitchen_Aide.  If not, see <http://www.gnu.org/licenses/>.
 */
