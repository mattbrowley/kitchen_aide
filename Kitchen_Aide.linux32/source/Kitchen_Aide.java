import processing.core.*; 
import processing.data.*; 
import processing.event.*; 
import processing.opengl.*; 

import java.util.HashMap; 
import java.util.ArrayList; 
import java.io.File; 
import java.io.BufferedReader; 
import java.io.PrintWriter; 
import java.io.InputStream; 
import java.io.OutputStream; 
import java.io.IOException; 

public class Kitchen_Aide extends PApplet {

/* Kitchen_Aide
 * Author: Matthew Rowley
 * Date Modified: 03/26/2014
 * 
 * A recipe manager and kitchen inventory designed for a small touchscreen.
 * This program will recall and display recipes stored in human-readable
 * plaintext files. Other useful kitchen features such as a recipe multiplier
 * and kitchen timer will be added later.
 *
 * Licence information is at the bottom
 */

ScreenData screenData; // This encapsulates the screen mode, page on each screen, and last button pressed
int mode;

RecipeLibrary recipes;
Recipe currentRecipe;
Ingredient currentIngredient;
float[] yieldMultipliers = {8,4,3,2.5f,2,1.75f,1.5f,1.25f,1,0.75f,0.666f,0.5f,0.333f,0.25f,0.125f};
PFont logoFont;
PFont smallLogoFont;
PFont boldFont;
PFont textFont;

Button[] buttons0 = new Button[5];
Button[] buttons1 = new Button[9];
Button[] buttons2 = new Button[9];
Button[] buttons3 = new Button[9];
Button[] buttons4 = new Button[9];
Button[] buttons5 = new Button[9];
Button[] buttons6 = new Button[3];
Button[] buttons7 = new Button[9];
Button[] buttons8 = new Button[4];
Button[][] Buttons = {buttons0, buttons1, buttons2, buttons3, buttons4, buttons5, buttons6, buttons7, buttons8};
String[] buttonNames;

public void setup(){
  noLoop();
  size(600,800);
  screenData = new ScreenData();
                                       // 0 - Home Screen
                                       // 1 - Ingredients
                                       // 2 - Types
                                       // 3 - Recipes by Name
                                       // 4 - Recipes by Ingredient
                                       // 5 - Recipes of Type
                                       // 6 - Recipe
                                       // 7 - Inventory
                                       // 8 - Add/Remove from Inventory
   recipes = new RecipeLibrary(screenData);
  logoFont = loadFont("TeXGyreChorus-MediumItalic-70.vlw");
  smallLogoFont = loadFont("TeXGyreTermes-Italic-25.vlw");
  boldFont = loadFont("Courier10PitchBT-Bold-30.vlw");
  textFont = loadFont("ArialMT-20.vlw");
  textAlign(CENTER, CENTER); // Position text in the center of buttons
   
  Buttons[0][0] = new ModeButton(width/2, 1*(height-300)/8+220 ,width/2 - 10, (height-340) / 8, "Recipes by Name", screenData, 3);
  Buttons[0][1] = new ModeButton(width/2, 3*(height-300)/8+220, width/2 - 10, (height-340) / 8, "Recipes by Ingredient", screenData, 1);
  Buttons[0][2] = new ModeButton(width/2, 5*(height-300)/8+220, width/2 - 10, (height-340) / 8, "Recipes by Type", screenData, 2);
  Buttons[0][3] = new ModeButton(width/2, 7*(height-300)/8+220, width/2 - 10, (height-340) / 8, "Manage Inventory", screenData, 7);
  Buttons[0][4] = new ExitButton(width - 45, height - 45, 35, 35, "Exit", screenData);
  
  for (int i = 1; i<3; i++){ // Set up buttons for the list selection screens (1-2)
    Buttons[i][0] = new UpButton(width/2-40, 40, width/2-60, 30, "Up", screenData);
    Buttons[i][1] = new DownButton(width/2-40, height-40, width/2-60, 30, "Down", screenData);
    Buttons[i][2] = new HomeButton(width-40, 40, 30, 30, "Home", screenData, 0);
    Buttons[i][3] = new ModeButton(width/2, 1*(height-150)/12+75, width/2 - 10, (height-190) / 12, "button "+ i + ".1", screenData, i+3);
    Buttons[i][4] = new ModeButton(width/2, 3*(height-150)/12+75, width/2 - 10, (height-190) / 12, "button "+ i + ".2", screenData, i+3);
    Buttons[i][5] = new ModeButton(width/2, 5*(height-150)/12+75, width/2 - 10, (height-190) / 12, "button "+ i + ".3", screenData, i+3);
    Buttons[i][6] = new ModeButton(width/2, 7*(height-150)/12+75, width/2 - 10, (height-190) / 12, "button "+ i + ".4", screenData, i+3);
    Buttons[i][7] = new ModeButton(width/2, 9*(height-150)/12+75, width/2 - 10, (height-190) / 12, "button "+ i + ".5", screenData, i+3);
    Buttons[i][8] = new ModeButton(width/2, 11*(height-150)/12+75, width/2 - 10, (height-190) / 12, "button "+ i + ".6", screenData, i+3);
  }
  for (int i = 3; i<6; i++){ // Set up buttons for the recipe selection screens (3-5)
    Buttons[i][0] = new UpButton(width/2-40, 40, width/2-60, 30, "Up", screenData);
    Buttons[i][1] = new DownButton(width/2-40, height-40, width/2-60, 30, "Down", screenData);
    Buttons[i][2] = new HomeButton(width-40, 40, 30, 30, "Home", screenData, 0);
    Buttons[i][3] = new ModeButton(width/2, 1*(height-150)/12+75, width/2 - 10, (height-190) / 12, "button "+ i + ".1", screenData, 6);
    Buttons[i][4] = new ModeButton(width/2, 3*(height-150)/12+75, width/2 - 10, (height-190) / 12, "button "+ i + ".2", screenData, 6);
    Buttons[i][5] = new ModeButton(width/2, 5*(height-150)/12+75, width/2 - 10, (height-190) / 12, "button "+ i + ".3", screenData, 6);
    Buttons[i][6] = new ModeButton(width/2, 7*(height-150)/12+75, width/2 - 10, (height-190) / 12, "button "+ i + ".4", screenData, 6);
    Buttons[i][7] = new ModeButton(width/2, 9*(height-150)/12+75, width/2 - 10, (height-190) / 12, "button "+ i + ".5", screenData, 6);
    Buttons[i][8] = new ModeButton(width/2, 11*(height-150)/12+75, width/2 - 10, (height-190) / 12, "button "+ i + ".6", screenData, 6);
  }
  Buttons[6][0] = new HomeButton(width-40, 40, 30, 30, "Home", screenData, 0);
  Buttons[6][1] = new UpButton(151, 118, 41, 19, "Up", screenData); // Increase yieldmultiplier
  Buttons[6][2] = new DownButton(100, 118, 41, 19, "Down", screenData); // Decrease yieldmultiplier
  Buttons[7][0] = new UpButton(width/2-40, 40, width/2-60, 30, "Up", screenData);
  Buttons[7][1] = new DownButton(width/2-40, height-40, width/2-60, 30, "Down", screenData);
  Buttons[7][2] = new HomeButton(width-40, 40, 30, 30, "Home", screenData, 0);
  Buttons[7][3] = new ModeButton(width/2, 1*(height-150)/12+75, width/2 - 10, (height-190) / 12, "button 7.1", screenData,8);
  Buttons[7][4] = new ModeButton(width/2, 3*(height-150)/12+75, width/2 - 10, (height-190) / 12, "button 7.2", screenData,8);
  Buttons[7][5] = new ModeButton(width/2, 5*(height-150)/12+75, width/2 - 10, (height-190) / 12, "button 7.3", screenData,8);
  Buttons[7][6] = new ModeButton(width/2, 7*(height-150)/12+75, width/2 - 10, (height-190) / 12, "button 7.4", screenData,8);
  Buttons[7][7] = new ModeButton(width/2, 9*(height-150)/12+75, width/2 - 10, (height-190) / 12, "button 7.5", screenData,8);
  Buttons[7][8] = new ModeButton(width/2, 11*(height-150)/12+75, width/2 - 10, (height-190) / 12, "button 7.6", screenData,8);
  Buttons[8][0] = new Button(width/2, height/2, width/2-10, 60, "Inventory Item", screenData);
  Buttons[8][1] = new UpButton(width/2, height/2-120, 120,50, "up", screenData);
  Buttons[8][2] = new DownButton(width/2, height/2+120, 120,50, "down", screenData);
  Buttons[8][3] = new BackButton(width-70, 70, 60, 40, "Back", screenData, 7, recipes);
  
 
    
  background(190);
  fill(50);
  redraw();
} // End setup()
public void draw(){
  background(190);
  if(mode != screenData.getMode()){ // 
    mode = screenData.getMode();
    if(mode > 0 && mode < 6){
      recipes.prepareMenu();
    }
    if(mode == 6){ // Set up some things for the drawRecipe() function
      currentRecipe = recipes.getRecipe(screenData.getLastPressed());
      screenData.setPage(8); // Start with a multiplier of 1
    }
    if(mode == 7){
      int pageMax = -1;
      for (int i = 0; i < recipes.getInventoryNumbers().length; i = i+6){
        pageMax++;
      } 
      screenData.setPageMax(pageMax);
    }
    if(mode == 8){
      currentIngredient = recipes.getIngredient(screenData.getLastPressed());
      screenData.setPage(999 - PApplet.parseInt(currentIngredient.getIngredientAmount()));
    }
  } // end if(modeChanged)
  if(mode==0){
    drawHome();
  }
  else if(mode==6){
    drawRecipe();
  }
  else if(mode==7){
    drawInventoryMenu();
  }
  else if(mode==8){
    drawChangeInventory();
  }
  else{
    drawMenu();
  }
  for (int i = 0; i < Buttons[mode].length; i++){
    Buttons[mode][i].drawButton();
  } 
} // End draw()

public void mousePressed(){
  for (int i = 0; i < Buttons[mode].length;i++){
    Buttons[mode][i].press(); // Button objects determine whether the mouse is over them when the mouse is pressed
  }
  redraw();
}

// Draw functions for each mode are below
public void drawHome(){
  fill(0,0,255);
  textFont(logoFont);
  text("Kitchen-Aide", width/2, 50);
  textFont(smallLogoFont);
  text("Recipe Manager and Kitchen Inventory", width/2, 110);
  textFont(boldFont);
  text("Written with Love by\nMatt Rowley", width/2, 180);
}
public void drawRecipe(){
  float multiplier = yieldMultipliers[screenData.getPage()];
  int stretchSpace = 0;
  fill(0);
  textFont(boldFont);
  textAlign(LEFT,CENTER);
  String recipeName = currentRecipe.getRecipeName();
  if(recipeName.length()<25){
    text(recipeName, 10, 40);
    textFont(smallLogoFont);
    text(currentRecipe.getRecipeType(), 10, 80+stretchSpace);
  }
  else{ // Break the title in to two lines in a logical way
    for(int i = 18; i < 30; i++){
      if(recipeName.charAt(i) == ' '){
        recipeName = recipeName.substring(0,i) + "\n" + recipeName.substring(i+1);
        break;
      }
      if(i == 30){
        recipeName = recipeName.substring(0,20) + "-\n" + recipeName.substring(20);
      }
    }
    text(recipeName, 10, 40);
    textFont(smallLogoFont);
    text(currentRecipe.getRecipeType(), 10, 85+stretchSpace);
  } 
  textFont(textFont);
  text(currentRecipe.getRecipeYield(multiplier), 10, 118+stretchSpace);
  strokeWeight(3);
  line(5,140+stretchSpace,width-5,140+stretchSpace);
  textFont(smallLogoFont);
  text("Ingredients:", 10, 160 + stretchSpace);
  textFont(textFont);
  for (int i = 0; i < currentRecipe.getIngredients().size(); i++){
    text(currentRecipe.getIngredients().get(i).getRecipeName(multiplier), 10, 190+stretchSpace);
    stretchSpace += 20;
  }
  line(5,190+stretchSpace,width-5,190+stretchSpace);
  textFont(smallLogoFont);
  text("Instructions:", 10, 210+stretchSpace);
  textFont(textFont);
  textAlign(LEFT,TOP);
  text(currentRecipe.getInstructions(), 10, 230+stretchSpace);
  
 
  strokeWeight(1);
  textAlign(CENTER,CENTER);
}
public void drawMenu(){ // Get the proper button names from the RecipeLibrary
  String[] buttonNames = recipes.getButtonNames();
  int page = screenData.getPage();
  for (int i = 0; i<6; i++){
    if(i+ 6*page < buttonNames.length){
      Buttons[mode][i+3].setButtonName(buttonNames[i+6*page]);
      Buttons[mode][i+3].setEnabled(true);
    }
    else{
      Buttons[mode][i+3].setEnabled(false);
    }
  } 
}
public void drawInventoryMenu(){
  String[] buttonNames = recipes.getInventoryNames();
  int[] inventoryNumbers = recipes.getInventoryNumbers();
  int page = screenData.getPage();
  for (int i = 0; i<6; i++){
    if(i+ 6*page < buttonNames.length){
      Buttons[mode][i+3].setButtonName(buttonNames[i+6*page]);
      Buttons[mode][i+3].setEnabled(true);
      int number = inventoryNumbers[i+6*page];
      if(number == 0){
        Buttons[mode][i+3].setColor(color(240,68,68));
      }
      else if(number == 1){
         Buttons[mode][i+3].setColor(color(240,125,68));
      }
      else{
         Buttons[mode][i+3].setColor(color(210, 210, 240));
      }
    }
    else{
      Buttons[mode][i+3].setEnabled(false);
    }
  } 
}
public void drawChangeInventory(){
  currentIngredient.setIngredientAmount(999 - screenData.getPage());
  int number = PApplet.parseInt(currentIngredient.getIngredientAmount());
    if(number == 0){
      Buttons[mode][0].setColor(color(240,68,68));
    }
    else if(number == 1){
       Buttons[mode][0].setColor(color(240,125,68));
    }
    else{
       Buttons[mode][0].setColor(color(210, 210, 240));
    }
  Buttons[mode][0].setButtonName(currentIngredient.getInventoryName());

}
/* This file is part of Kitchen_Aide.
 *
 * Kitchen_Aide is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Kitchen_Aide is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser Public License for more details.
 *
 * You should have received a copy of the GNU Lesser Public License
 * along with Kitchen_Aide.  If not, see <http://www.gnu.org/licenses/>.
 */
/* Button Class
 * Author: Matthew Rowley
 * 
 * This class is a generalized button
 *
 * Licence information is at the bottom
 */

class Button {
  int xpos; // Note that these are center positions
  int ypos;
  int wideness; // Note that these are "radii" (1/2 the wideness and 1/2 the highness
  int highness;
  int buttonColor;
  String name; // The text displayed on the button and an identifier to signal what button was last pressed
  boolean enabled; // A button can be disabled so it isn't displayed and cannot act
  ScreenData screenData;
  // Dummy Constructor
  Button() {
    enabled = false;
  }
  // Main Constructor
  Button(int ixpos, int iypos, int iwide, int ihigh, String ina, ScreenData isd) {
    xpos = ixpos;
    ypos = iypos;
    wideness = iwide;
    highness = ihigh;
    name = ina;
    screenData = isd;
    buttonColor = color(210, 210, 240);
    enabled = true; // Allows a button to not be drawn if false
  }
  public void press() { // Checks if the mouse is over this particular button, and the button is enabled
    if (mouseX>xpos-wideness && mouseX<xpos+wideness && mouseY>ypos-highness && mouseY<ypos+highness && enabled) {
      act();
    }
  }
  public void setButtonName(String setValue) {
    name = setValue;
  }
  public void act() {
    screenData.setLastPressed(name);
  }
  public void setEnabled(boolean setValue) {
    enabled = setValue;
  }
  public void setColor(int setColor){
    buttonColor = setColor;
  }
  public void drawButton() {
    if (enabled) {
      fill(buttonColor);
      rect(xpos-wideness, ypos-highness, 2 * wideness, 2 * highness, 20, 20, 20, 20);
      fill(50); // Dark Gray Letters
      textFont(boldFont);
     // textAlign(LEFT,CENTER);
      String displayName = name;
      boolean splitName = false;
      for (int i = 0; i<name.length(); i++){
        if (name.charAt(i) == '\n'){
          splitName = true;
        }
      }
      if (name.length()>25 && !splitName){ // Break the name in to two lines in a logical way
        for (int i = 18; i < 30; i++) {
          if (name.charAt(i) == ' ') {
            displayName = name.substring(0, i) + "\n" + name.substring(i+1);
            break;
          }
          if (i == 30) {
            displayName = name.substring(0, 20) + "-\n" + name.substring(20);
          }
        }
      }
      text(displayName, xpos, ypos);
     // textAlign(CENTER,CENTER);
    }
  }
}


/* This file is part of Kitchen_Aide.
 *
 * Kitchen_Aide is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Kitchen_Aide is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser Public License for more details.
 *
 * You should have received a copy of the GNU Lesser Public License
 * along with Kitchen_Aide.  If not, see <http://www.gnu.org/licenses/>.
 */
/* Button Classes
 * Author: Matthew Rowley
 * 
 * These are the various specialized button classes
 *
 * Licence information is at the bottom
 */
 
class ModeButton extends Button {
  int targetMode;
  ModeButton(int ixpos, int iypos, int iwide, int ihigh, String ina, ScreenData isd, int itmode) {
    super(ixpos, iypos, iwide, ihigh, ina, isd);
    targetMode = itmode;
  }
  public void act() {
    screenData.setMode(targetMode);
    screenData.setPage(0);
    screenData.setLastPressed(name);
  }
}
class HomeButton extends ModeButton {
  HomeButton(int ixpos, int iypos, int iwide, int ihigh, String ina,  ScreenData isd, int itmode) {
    super(ixpos, iypos, iwide, ihigh, ina, isd, itmode);
    buttonColor = color(229, 51, 51);
  }
  public void drawButton() {
    fill(buttonColor);
    beginShape(); // Chimney
    vertex(xpos+wideness/1.5f, ypos-highness);
    vertex(xpos+wideness/3, ypos-highness);
    vertex(xpos+wideness/3, ypos);
    vertex(xpos+wideness/1.5f, ypos);
    vertex(xpos+wideness/1.5f, ypos-highness);
    endShape();
    beginShape(); // House
    vertex(xpos, ypos-highness);
    vertex(xpos+wideness, ypos-highness/4);
    vertex(xpos+wideness/1.25f, ypos-highness/4);
    vertex(xpos+wideness/1.25f, ypos+highness);
    vertex(xpos-wideness/1.25f, ypos+highness);
    vertex(xpos-wideness/1.25f, ypos-highness/4);
    vertex(xpos-wideness, ypos-highness/4);
    vertex(xpos, ypos-highness);
    endShape();
    beginShape(); // Door
    vertex(xpos+wideness/4, ypos+highness);
    vertex(xpos+wideness/4, ypos+highness/4);
    vertex(xpos-wideness/4, ypos+highness/4);
    vertex(xpos-wideness/4, ypos+highness);
    vertex(xpos+wideness/4, ypos+highness);
    endShape();
  }
  public void act() {
    screenData.setMode(targetMode);
    screenData.setLastPressed(name);
    noLoop(); // Sets us back to no loop function in case we were looping for a timer
  }
}
class ExitButton extends Button {
  ExitButton(int ixpos, int iypos, int iwide, int ihigh, String ina, ScreenData isd) {
    super(ixpos, iypos, iwide, ihigh, ina, isd);
    buttonColor = color(229, 51, 51);
  }
  public void act() {
    exit();
  }
  public void drawButton() {
    fill(buttonColor);
    beginShape();
    vertex(xpos+wideness/1.2f, ypos+highness);
    vertex(xpos+wideness/1.7f, ypos+highness);
    vertex(xpos+wideness/1.7f, ypos-highness/2);
    vertex(xpos-wideness/1.7f, ypos-highness/2);
    vertex(xpos-wideness/1.7f, ypos+highness);
    vertex(xpos-wideness/1.2f, ypos+highness);
    vertex(xpos-wideness/1.2f, ypos-highness);
    vertex(xpos+wideness/1.2f, ypos-highness);
    vertex(xpos+wideness/1.2f, ypos+highness);
    endShape();
    beginShape();
    vertex(xpos+wideness/1.7f-1, ypos+highness);
    vertex(xpos+wideness/1.7f-1, ypos-highness/2);
    vertex(xpos-wideness/2, ypos-highness/1.75f);
    vertex(xpos-wideness/2, ypos+highness*1.25f);
    vertex(xpos+wideness/1.7f-1, ypos+highness);
    endShape();
    ellipse(xpos-wideness/5,ypos+highness/2.5f,4,4);
  }
}
class UpButton extends Button {
  UpButton(int ixpos, int iypos, int iwide, int ihigh, String ina, ScreenData isd) {
    super(ixpos, iypos, iwide, ihigh, ina, isd);
    buttonColor = color(9, 134, 25);
  }
  public void drawButton() {
    if (screenData.getPage()>0) {
      fill(buttonColor);
      triangle(xpos-wideness, ypos+highness, xpos, ypos-highness, xpos+wideness, ypos+highness);
    }
  }
  public void press() { // Checks if the mouse is over this particular button, and the button is enabled
    if (mouseX>(xpos-wideness+wideness/2/highness*(ypos+highness-mouseY)) && mouseX<(xpos+wideness-wideness/2/highness*(ypos+highness-mouseY)) && mouseY<ypos+highness && enabled) {
      act();
    }
  }
  public void act() {
    screenData.decrementPage();
  }
}
class DownButton extends Button {
  DownButton(int ixpos, int iypos, int iwide, int ihigh, String ina, ScreenData isd) {
    super(ixpos, iypos, iwide, ihigh, ina, isd);
    buttonColor = color(9, 134, 25);
  }
  public void drawButton() {
    if (screenData.getPage()<screenData.getPageMax()) {
      fill(buttonColor);
      triangle(xpos-wideness, ypos-highness, xpos, ypos+highness, xpos+wideness, ypos-highness);
    }
  }
  public void press() { // Checks if the mouse is over this particular button, and the button is enabled
    if (mouseX>(xpos-wideness+wideness/2/highness*(mouseY-ypos+highness)) && mouseX<(xpos+wideness-wideness/2/highness*(mouseY-ypos+highness)) && mouseY>ypos-highness && enabled) {
      act();
    }
  }
  public void act() {
    screenData.incrementPage();
  }
}
class BackButton extends ModeButton {
  RecipeLibrary recipes;
  String[] buttonNames;
  BackButton(int ixpos, int iypos, int iwide, int ihigh, String ina, ScreenData isd, int itmode, RecipeLibrary irl) {
    super(ixpos, iypos, iwide, ihigh, ina, isd, itmode);
    recipes = irl;
  }
  public void act(){
    screenData.setMode(targetMode);
    recipes.writeInventory();
  }
}

/* This file is part of Kitchen_Aide.
 *
 * Kitchen_Aide is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Kitchen_Aide is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser Public License for more details.
 *
 * You should have received a copy of the GNU Lesser Public License
 * along with Kitchen_Aide.  If not, see <http://www.gnu.org/licenses/>.
 */
/* Ingredient Class
 * Author: Matthew Rowley
 * 
 * This class stores information about an ingredient
 *
 * Licence information is at the bottom
 */
 
 class Ingredient{
   String ingredientName;
   float ingredientAmount;
   String ingredientUnit;
   String ingredientUnits;
   Ingredient(String line){
     String[] lineTokens = split(line, " - ");
     ingredientName = lineTokens[0];
     lineTokens = split(lineTokens[1], '[');
     ingredientAmount = PApplet.parseFloat(lineTokens[0]);
     ingredientUnit = lineTokens[1].substring(0,lineTokens[1].length()-1);
     ingredientUnits = lineTokens[2].substring(0,lineTokens[2].length()-1);
   }
   public String getRawData(){
     String data = ingredientName + " - " + PApplet.parseInt(ingredientAmount) + " [" + ingredientUnit + "][" + ingredientUnits +"]";
     return data;
   }
   public String getIngredientName(){
     return ingredientName;
   }
   public String getInventoryName(){
     String name;
     if (ingredientAmount == 1){
       name = ingredientName + "-\n" + PApplet.parseInt(ingredientAmount) + " " + ingredientUnit;
     }
     else{
       name = ingredientName + "-\n" + PApplet.parseInt(ingredientAmount) + " " + ingredientUnits;
     }
     return name;
   }
   public String getRecipeName(float multiplier){
     String name;
     if (formatNumber(ingredientAmount * multiplier).equals("1") || ingredientAmount * multiplier <= 0.59f ){
       name = ingredientName + " - " + formatNumber(ingredientAmount*multiplier) + " " + ingredientUnit;
     }
     else{
       name = ingredientName + " - " + formatNumber(ingredientAmount*multiplier) + " " + ingredientUnits;
     }
     return name;
   }
   public float getIngredientAmount(){
     return ingredientAmount;
   }
   public void setIngredientAmount(int amount){
     ingredientAmount = PApplet.parseFloat(amount);
   }
   public String formatNumber(float number){
     float remainder = number - PApplet.parseInt(number);
     String remainderString;
     if(remainder <0.075f && PApplet.parseInt(number) != 0){
       remainderString = "";
     }
     else if(remainder <= 0.175f){
       remainderString = " 1/8";
     }
     else if(remainder <= 0.29f){
       remainderString = " 1/4";
     }
     else if(remainder <= 0.41f){
       remainderString = " 1/3";
     }
     else if(remainder <= 0.59f){
       remainderString = " 1/2";
     }
     else if(remainder <= 0.71f){
       remainderString = " 2/3";
     }
     else if(remainder <= 0.85f){
       remainderString = " 3/4";
     }
     else{
       number++;
       remainderString = "";
     }
     if(PApplet.parseInt(number)== 0){
       return remainderString.substring(1);
     }
     else{
       return PApplet.parseInt(number) + remainderString;
     }
   }
 }
 
/* This file is part of Kitchen_Aide.
 *
 * Kitchen_Aide is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Kitchen_Aide is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser Public License for more details.
 *
 * You should have received a copy of the GNU Lesser Public License
 * along with Kitchen_Aide.  If not, see <http://www.gnu.org/licenses/>.
 */
/* Recipe Class
 * Author: Matthew Rowley
 * 
 * This class contains all the information about a recipe
 *
 * Licence information is at the bottom
 */
 class Recipe{
   String[] fileLines;
   String recipeName;
   String recipeType;
   float recipeYield;
   String yieldUnit;
   String yieldUnits;
   ArrayList<Ingredient> ingredients;
   String instructions;
   int currentLine; // the first line of ingredients
   String[] lineTokens;
   Recipe(File recipeFile){
     instructions = "";
     ingredients = new ArrayList<Ingredient>();
     fileLines = loadStrings(recipeFile.getAbsolutePath());
     recipeName = fileLines[1];
     recipeType = fileLines[3];
     lineTokens = split(fileLines[5], '[');
     recipeYield = parseFloat(split(lineTokens[0],' ')[1]);
     yieldUnit = lineTokens[1].substring(0,lineTokens[1].length()-1);
     yieldUnits = lineTokens[2].substring(0,lineTokens[2].length()-1);
     currentLine = 8;
     while(!fileLines[currentLine].equals("-----------------------------------------------------------------")){
       ingredients.add(new Ingredient(fileLines[currentLine]));
       currentLine++;
     }
     currentLine = currentLine + 2;
     while(!fileLines[currentLine].equals("=================================================================")&&currentLine<fileLines.length){ // Check the current line in case the .rcp file doesn't end in a line of dashes
       instructions = instructions + fileLines[currentLine] + "\n";
       currentLine++;
     }
   }
   public String getRecipeName(){
     return recipeName;
   }
   public String getRecipeType(){
     return recipeType;
   }
   public String getRecipeYield(float multiplier){
     String yield;
     float number = recipeYield*multiplier;
     if(number == 1 || number <= 0.59f){
       yield = "Yields:                            " + formatNumber(number) + " " + yieldUnit;
     }
     else{
       yield = "Yields:                            " + formatNumber(number) + " " + yieldUnits;
     }
     if(multiplier != 1){
       yield = yield + " (x" + formatNumber(multiplier) + ")";
     }
     return yield;
   }
   
   public ArrayList<Ingredient> getIngredients(){
     return ingredients;
   }
   public boolean hasIngredient(String ingredient){
     boolean doesHave = false;
     for (int i = 0; i<ingredients.size(); i++){
       doesHave = doesHave || ingredients.get(i).getIngredientName().equals(ingredient);
     }
     return doesHave;
   }
   public String getInstructions(){
     return instructions;
   }
   public String formatNumber(float number){
     float remainder = number - PApplet.parseInt(number);
     String remainderString;
     if(remainder <0.075f){
       remainderString = "";
     }
     else if(remainder <= 0.175f){
       remainderString = " 1/8";
     }
     else if(remainder <= 0.29f){
       remainderString = " 1/4";
     }
     else if(remainder <= 0.41f){
       remainderString = " 1/3";
     }
     else if(remainder <= 0.59f){
       remainderString = " 1/2";
     }
     else if(remainder <= 0.71f){
       remainderString = " 2/3";
     }
     else if(remainder <= 0.85f){
       remainderString = " 3/4";
     }
     else{
       number++;
       remainderString = "";
     }
     if(PApplet.parseInt(number)== 0){
       return remainderString.substring(1);
     }
     else{
       return PApplet.parseInt(number) + remainderString;
     }
   }
 }
 
/* This file is part of Kitchen_Aide.
 *
 * Kitchen_Aide is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Kitchen_Aide is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser Public License for more details.
 *
 * You should have received a copy of the GNU Lesser Public License
 * along with Kitchen_Aide.  If not, see <http://www.gnu.org/licenses/>.
 */
/* RecipeLibrary Class
 * Author: Matthew Rowley
 * 
 * This class will import all recipes and ingredients from human-
 * readable text files. It will also provide the appropriate names
 * to display on list selection buttons.
 *
 * Licence information is at the bottom
 */

class RecipeLibrary {
  ScreenData screenData;
  ArrayList<Recipe> recipeList;
  StringList recipes;
  StringList ingredients;
  StringList types;
  StringList[] recipesByIngredient;
  StringList[] recipesByType;
  StringList currentMenu;
  ArrayList<Ingredient> inventory;
  // Constructor
  RecipeLibrary(ScreenData isd) {
    screenData = isd;
    recipeList = new ArrayList<Recipe>();
    recipes = new StringList();
    ingredients = new StringList();
    types = new StringList(); 
    currentMenu = new StringList();
    // Import all the recipe files in the recipe folder
    File recipeFolder = new File(sketchPath("Recipes"));
    getRecipeFiles(recipeFolder);

    // Create tier 1 button name lists
    for (int i = 0; i<recipeList.size(); i++) { 
      Recipe thisRecipe = recipeList.get(i);
      String thisName = thisRecipe.getRecipeName();
      ArrayList<Ingredient> theseIngredients = thisRecipe.getIngredients();
      String thisType = thisRecipe.getRecipeType();
      if (!recipes.hasValue(thisName)) { // repeat recipe names will not be added
        recipes.append(thisName);
        for (int j = 0; j<theseIngredients.size(); j++) {
          if (!ingredients.hasValue(theseIngredients.get(j).getIngredientName())) {
            ingredients.append(theseIngredients.get(j).getIngredientName());
          }
        }
        if (!types.hasValue(thisType)) {
          types.append(thisType);
        }
      } // end of if(!recipes.hasValue(thisName))
      else { 
        String removedName = recipeList.remove(i).getRecipeName(); // Here repeat names are removed from the recipeList
        println("The recipe: " + removedName + " was not added because it was a duplicate");
        i--; // i is decremented so it will not increment - the element was removed so we don't actually want the next index
      }
    } // end of creating tier 1 lists
    recipes.sort();
    ingredients.sort();

    // Create tier 2 button name lists
    recipesByIngredient = new StringList[ingredients.size()];
    for (int i = 0; i<recipesByIngredient.length;i++) {
      recipesByIngredient[i] = new StringList();
    }
    // Get recipes by ingredient
    for (int i = 0; i < ingredients.size(); i++) {
      for (int j = 0; j < recipes.size(); j++) {
        Recipe thisRecipe = recipeList.get(j);
        if (thisRecipe.hasIngredient(ingredients.get(i))) {
          recipesByIngredient[i].append(thisRecipe.getRecipeName());
        }
      }
      recipesByIngredient[i].sort();
    }
    types.sort();
    recipesByType = new StringList[types.size()];
    for (int i = 0; i<recipesByType.length;i++) {
      recipesByType[i] = new StringList();
    }
    // Get recipes by type
    for (int i = 0; i < types.size(); i++) {
      for (int j = 0; j < recipes.size(); j++) {
        Recipe thisRecipe = recipeList.get(j);
        if (thisRecipe.getRecipeType().equals(types.get(i))) {
          recipesByType[i].append(thisRecipe.getRecipeName());
        }
      }
      recipesByType[i].sort();
    }

    importInventory();
  }// End constructor

  public void getRecipeFiles(File folder) {
    String[] names = folder.list();
    for (int i=0; i<names.length; i++) {
      File thisFile = new File(folder, names[i]);
      if (thisFile.isDirectory()) {
        getRecipeFiles(thisFile);
      }
      else if (names[i].substring(names[i].length()-4, names[i].length()).equals(".rcp")) {
        recipeList.add(new Recipe(thisFile));
      }
    }
  }
  public void importInventory() {
    inventory = new ArrayList<Ingredient>();
    File inventoryFolder = new File(sketchPath("Inventory"));
    File inventoryFile = new File(inventoryFolder, "Inventory.inv");
    String[] lines = loadStrings(inventoryFile);
    int currentLine = 2;
    while (!lines[currentLine].equals ("--------------------------------------------------")) {
      inventory.add(new Ingredient(lines[currentLine]));
      currentLine++;
    }
  }
  public Recipe getRecipe(String recipeName) {
    Recipe temp = recipeList.get(0);
    for (int i = 0; i<recipeList.size();i++) {
      temp = recipeList.get(i);
      if (temp.getRecipeName().equals(recipeName)) {
        break;
      }
    }
    return temp;
  }
  public void prepareMenu() {
    if (screenData.getMode() == 1) {
      currentMenu = ingredients;
    }
    else if (screenData.getMode() == 2) {
      currentMenu = types;
    }
    else if (screenData.getMode() == 3) {
      currentMenu = recipes;
    }
    else if (screenData.getMode() == 4) {
      String lastPressed = screenData.getLastPressed();
      int index = 0;
      for (int i = 0; i< ingredients.size(); i++) {
        if (ingredients.get(i).equals(lastPressed)) {
          index = i;
        }
      }
      currentMenu = recipesByIngredient[index];
    }
    else if (screenData.getMode() == 5) {
      String lastPressed = screenData.getLastPressed();
      int index = 0;
      for (int i = 0; i< types.size(); i++) {
        if (types.get(i).equals(lastPressed)) {
          index = i;
        }
      }
      currentMenu = recipesByType[index];
    }
    int pageMax = -1;
    for (int i = 0; i < currentMenu.size(); i = i +6){
      pageMax++;
    } 
    screenData.setPageMax(pageMax);
  }
  public Ingredient getIngredient(String name){
    Ingredient thisIngredient = new Ingredient("Fake - 1 [fake][fake]");
    for(int i = 0; i<inventory.size(); i++){
      if(name.equals(inventory.get(i).getInventoryName())){
        thisIngredient = inventory.get(i);
      }  
    }
    return thisIngredient;
  }
  public String[] getInventoryNames() {
    String[] names = new String[inventory.size()];
    for (int i = 0; i< inventory.size(); i++) {
      names[i] = inventory.get(i).getInventoryName();
    }
    return names;
  }
  public int[] getInventoryNumbers() {
    int[] numbers = new int[inventory.size()];
    for (int i = 0; i< inventory.size(); i++) {
      numbers[i] = PApplet.parseInt(inventory.get(i).getIngredientAmount());
    }
    return numbers;
  }
  public void writeInventory(){
    File inventoryFile = new File(sketchPath("Inventory/Inventory.inv"));
    String[] lines = loadStrings(inventoryFile);
    int currentLine = 2;
    while (!lines[currentLine].equals ("--------------------------------------------------")) {
      lines[currentLine] = inventory.get(currentLine-2).getRawData();
      currentLine++;
    }
    saveStrings(inventoryFile.getAbsolutePath(), lines);
  }
  public String[] getButtonNames() {
    String[] buttonNames = currentMenu.array();
    return buttonNames;
  }
}

/* This file is part of Kitchen_Aide.
 *
 * Kitchen_Aide is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Kitchen_Aide is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser Public License for more details.
 *
 * You should have received a copy of the GNU Lesser Public License
 * along with Kitchen_Aide.  If not, see <http://www.gnu.org/licenses/>.
 */
/* ScreenData Class
 * Author: Matthew Rowley
 * 
 * This class holds global variables used by other classes in the
 * Kitchen_Aide program. Namely, the screen mode, current page
 * for each screen, and name of the last button pushed. 
 *
 * Licence information is at the bottom
 */

class ScreenData{
  int mode;
  int[] pages = {0,0,0,0,0,0,8,0,999};
  int[] pagesMax = {0,0,0,0,0,0,14,0,999};
  boolean modeChanged;
  boolean pageChanged;
  String lastPressed;
  String currentRecipe;
  float yieldMultiplier;
  public void ScreenData(){
    mode = 0;
    modeChanged = true;
    pageChanged = true;
    lastPressed = "Unchanged";
    currentRecipe = "Chicken Enchiladas"; // Perhaps change this to an emptry string once debugging is over
  }
  public int getMode(){
    modeChanged = false;
    return mode;
  }
  public void setMode(int setValue){
    mode = setValue;
    modeChanged = true;
  }
  public boolean getModeChanged(){
    return modeChanged;
  }
  public int getPage(){
    return pages[mode];
  }
  public void setPage(int setValue){
    pages[mode] = setValue;
    pageChanged = true;
  }
  public void incrementPage(){
    if (pages[mode]<pagesMax[mode]){
      pages[mode]++;
      pageChanged = true;
    }
  }
  public void decrementPage(){
    if (pages[mode]>0){
      pages[mode]--;
      pageChanged = true;
    }
  }
  public boolean getPageChanged(){
    return pageChanged;
  }
  public void clearPageChanged(){
    pageChanged = false;
  }
  public int getPageMax(){
    return pagesMax[mode];
  }
  public void setPageMax(int setValue){
    pagesMax[mode] = setValue;
  }
  public String getLastPressed(){
    return lastPressed;
  }
  public void setLastPressed(String setValue){
    lastPressed = setValue;
  }
  public String getCurrentRecipe(){
    return currentRecipe;
  }
  public void selectCurrentRecipe(){
    currentRecipe = lastPressed;
  }
}

/* This file is part of Kitchen_Aide.
 *
 * Kitchen_Aide is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Kitchen_Aide is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser Public License for more details.
 *
 * You should have received a copy of the GNU Lesser Public License
 * along with Kitchen_Aide.  If not, see <http://www.gnu.org/licenses/>.
 */
  static public void main(String[] passedArgs) {
    String[] appletArgs = new String[] { "--full-screen", "--bgcolor=#666666", "--hide-stop", "Kitchen_Aide" };
    if (passedArgs != null) {
      PApplet.main(concat(appletArgs, passedArgs));
    } else {
      PApplet.main(appletArgs);
    }
  }
}
