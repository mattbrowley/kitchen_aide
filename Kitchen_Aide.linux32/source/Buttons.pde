/* Button Classes
 * Author: Matthew Rowley
 * 
 * These are the various specialized button classes
 *
 * Licence information is at the bottom
 */
 
class ModeButton extends Button {
  int targetMode;
  ModeButton(int ixpos, int iypos, int iwide, int ihigh, String ina, ScreenData isd, int itmode) {
    super(ixpos, iypos, iwide, ihigh, ina, isd);
    targetMode = itmode;
  }
  void act() {
    screenData.setMode(targetMode);
    screenData.setPage(0);
    screenData.setLastPressed(name);
  }
}
class HomeButton extends ModeButton {
  HomeButton(int ixpos, int iypos, int iwide, int ihigh, String ina,  ScreenData isd, int itmode) {
    super(ixpos, iypos, iwide, ihigh, ina, isd, itmode);
    buttonColor = color(229, 51, 51);
  }
  void drawButton() {
    fill(buttonColor);
    beginShape(); // Chimney
    vertex(xpos+wideness/1.5, ypos-highness);
    vertex(xpos+wideness/3, ypos-highness);
    vertex(xpos+wideness/3, ypos);
    vertex(xpos+wideness/1.5, ypos);
    vertex(xpos+wideness/1.5, ypos-highness);
    endShape();
    beginShape(); // House
    vertex(xpos, ypos-highness);
    vertex(xpos+wideness, ypos-highness/4);
    vertex(xpos+wideness/1.25, ypos-highness/4);
    vertex(xpos+wideness/1.25, ypos+highness);
    vertex(xpos-wideness/1.25, ypos+highness);
    vertex(xpos-wideness/1.25, ypos-highness/4);
    vertex(xpos-wideness, ypos-highness/4);
    vertex(xpos, ypos-highness);
    endShape();
    beginShape(); // Door
    vertex(xpos+wideness/4, ypos+highness);
    vertex(xpos+wideness/4, ypos+highness/4);
    vertex(xpos-wideness/4, ypos+highness/4);
    vertex(xpos-wideness/4, ypos+highness);
    vertex(xpos+wideness/4, ypos+highness);
    endShape();
  }
  void act() {
    screenData.setMode(targetMode);
    screenData.setLastPressed(name);
    noLoop(); // Sets us back to no loop function in case we were looping for a timer
  }
}
class ExitButton extends Button {
  ExitButton(int ixpos, int iypos, int iwide, int ihigh, String ina, ScreenData isd) {
    super(ixpos, iypos, iwide, ihigh, ina, isd);
    buttonColor = color(229, 51, 51);
  }
  void act() {
    exit();
  }
  void drawButton() {
    fill(buttonColor);
    beginShape();
    vertex(xpos+wideness/1.2, ypos+highness);
    vertex(xpos+wideness/1.7, ypos+highness);
    vertex(xpos+wideness/1.7, ypos-highness/2);
    vertex(xpos-wideness/1.7, ypos-highness/2);
    vertex(xpos-wideness/1.7, ypos+highness);
    vertex(xpos-wideness/1.2, ypos+highness);
    vertex(xpos-wideness/1.2, ypos-highness);
    vertex(xpos+wideness/1.2, ypos-highness);
    vertex(xpos+wideness/1.2, ypos+highness);
    endShape();
    beginShape();
    vertex(xpos+wideness/1.7-1, ypos+highness);
    vertex(xpos+wideness/1.7-1, ypos-highness/2);
    vertex(xpos-wideness/2, ypos-highness/1.75);
    vertex(xpos-wideness/2, ypos+highness*1.25);
    vertex(xpos+wideness/1.7-1, ypos+highness);
    endShape();
    ellipse(xpos-wideness/5,ypos+highness/2.5,4,4);
  }
}
class UpButton extends Button {
  UpButton(int ixpos, int iypos, int iwide, int ihigh, String ina, ScreenData isd) {
    super(ixpos, iypos, iwide, ihigh, ina, isd);
    buttonColor = color(9, 134, 25);
  }
  void drawButton() {
    if (screenData.getPage()>0) {
      fill(buttonColor);
      triangle(xpos-wideness, ypos+highness, xpos, ypos-highness, xpos+wideness, ypos+highness);
    }
  }
  void press() { // Checks if the mouse is over this particular button, and the button is enabled
    if (mouseX>(xpos-wideness+wideness/2/highness*(ypos+highness-mouseY)) && mouseX<(xpos+wideness-wideness/2/highness*(ypos+highness-mouseY)) && mouseY<ypos+highness && enabled) {
      act();
    }
  }
  void act() {
    screenData.decrementPage();
  }
}
class DownButton extends Button {
  DownButton(int ixpos, int iypos, int iwide, int ihigh, String ina, ScreenData isd) {
    super(ixpos, iypos, iwide, ihigh, ina, isd);
    buttonColor = color(9, 134, 25);
  }
  void drawButton() {
    if (screenData.getPage()<screenData.getPageMax()) {
      fill(buttonColor);
      triangle(xpos-wideness, ypos-highness, xpos, ypos+highness, xpos+wideness, ypos-highness);
    }
  }
  void press() { // Checks if the mouse is over this particular button, and the button is enabled
    if (mouseX>(xpos-wideness+wideness/2/highness*(mouseY-ypos+highness)) && mouseX<(xpos+wideness-wideness/2/highness*(mouseY-ypos+highness)) && mouseY>ypos-highness && enabled) {
      act();
    }
  }
  void act() {
    screenData.incrementPage();
  }
}
class BackButton extends ModeButton {
  RecipeLibrary recipes;
  String[] buttonNames;
  BackButton(int ixpos, int iypos, int iwide, int ihigh, String ina, ScreenData isd, int itmode, RecipeLibrary irl) {
    super(ixpos, iypos, iwide, ihigh, ina, isd, itmode);
    recipes = irl;
  }
  void act(){
    screenData.setMode(targetMode);
    recipes.writeInventory();
  }
}

/* This file is part of Kitchen_Aide.
 *
 * Kitchen_Aide is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Kitchen_Aide is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser Public License for more details.
 *
 * You should have received a copy of the GNU Lesser Public License
 * along with Kitchen_Aide.  If not, see <http://www.gnu.org/licenses/>.
 */
