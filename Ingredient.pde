/* Ingredient Class
 * Author: Matthew Rowley
 * 
 * This class stores information about an ingredient
 *
 * Licence information is at the bottom
 */
 
 class Ingredient{
   String ingredientName;
   float ingredientAmount;
   String ingredientUnit;
   String ingredientUnits;
   Ingredient(String line){
     String[] lineTokens = split(line, " - ");
     ingredientName = lineTokens[0];
     lineTokens = split(lineTokens[1], '[');
     ingredientAmount = float(lineTokens[0]);
     ingredientUnit = lineTokens[1].substring(0,lineTokens[1].length()-1);
     ingredientUnits = lineTokens[2].substring(0,lineTokens[2].length()-1);
   }
   String getRawData(){
     String data = ingredientName + " - " + int(ingredientAmount) + " [" + ingredientUnit + "][" + ingredientUnits +"]";
     return data;
   }
   String getIngredientName(){
     return ingredientName;
   }
   String getInventoryName(){
     String name;
     if (ingredientAmount == 1){
       name = ingredientName + "-\n" + int(ingredientAmount) + " " + ingredientUnit;
     }
     else{
       name = ingredientName + "-\n" + int(ingredientAmount) + " " + ingredientUnits;
     }
     return name;
   }
   String getRecipeName(float multiplier){
     String name;
     if (formatNumber(ingredientAmount * multiplier).equals("1") || ingredientAmount * multiplier <= 0.59 ){
       name = ingredientName + " - " + formatNumber(ingredientAmount*multiplier) + " " + ingredientUnit;
     }
     else{
       name = ingredientName + " - " + formatNumber(ingredientAmount*multiplier) + " " + ingredientUnits;
     }
     return name;
   }
   float getIngredientAmount(){
     return ingredientAmount;
   }
   void setIngredientAmount(int amount){
     ingredientAmount = float(amount);
   }
   String formatNumber(float number){
     float remainder = number - int(number);
     String remainderString;
     if(remainder <0.075 && int(number) != 0){
       remainderString = "";
     }
     else if(remainder <= 0.175){
       remainderString = " 1/8";
     }
     else if(remainder <= 0.29){
       remainderString = " 1/4";
     }
     else if(remainder <= 0.41){
       remainderString = " 1/3";
     }
     else if(remainder <= 0.59){
       remainderString = " 1/2";
     }
     else if(remainder <= 0.71){
       remainderString = " 2/3";
     }
     else if(remainder <= 0.85){
       remainderString = " 3/4";
     }
     else{
       number++;
       remainderString = "";
     }
     if(int(number)== 0){
       return remainderString.substring(1);
     }
     else{
       return int(number) + remainderString;
     }
   }
 }
 
/* This file is part of Kitchen_Aide.
 *
 * Kitchen_Aide is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Kitchen_Aide is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser Public License for more details.
 *
 * You should have received a copy of the GNU Lesser Public License
 * along with Kitchen_Aide.  If not, see <http://www.gnu.org/licenses/>.
 */
